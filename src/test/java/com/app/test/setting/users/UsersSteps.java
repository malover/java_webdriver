package com.app.test.setting.users;



import com.app.test.steps.BaseSteps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * Created by ALyubozhenko
 */
@Component
public class UsersSteps extends BaseSteps {

    private static final Logger logger = LoggerFactory.getLogger(UsersSteps.class);


    private static final String[] ALL_ACCESS_ROLES = {};

}
