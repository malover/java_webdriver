package com.app.test.setting.users;

import com.app.test.BaseWebDriverTest;
import com.app.test.model.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by ALyubozhenko
 */
@Component
@Features("Work with users")
@Stories("Check abilities to login on behalf of different users")
public class TestUsersCreation
        extends BaseWebDriverTest {

    private User adminUser;

    @Autowired
    private UsersSteps usersSteps;

    @BeforeMethod
    public void setUpBeforeMethod() {
        adminUser = User.getAdminUser();
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Description("Check that it's possible to login for user with admin role")
    public void shouldBeAbleToLoginOnBehalfOfAdmin() {
        usersSteps.openPage(startUrl);
        usersSteps.login(adminUser);
    }

}
