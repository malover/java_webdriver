package com.app.test;

import com.app.test.config.ConfigurationProperties;
import com.app.test.config.WorkBeanConfiguration;
import com.app.test.steps.BaseSteps;
import com.app.test.testng.Report_ITestListener;
import com.app.test.core.DriverManager;
import com.app.test.core.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

/**
 * Created by ALyubozhenko
 */

//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = {WorkBeanConfiguration.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@Listeners(Report_ITestListener.class)
public abstract class BaseWebDriverTest
        extends AbstractTestNGSpringContextTests {


    protected final static String TEST_NAME = "TEST_";
    protected final static boolean AVAILABLE = true;
    protected final static boolean UNAVAILABLE = false;
    protected static String startUrl;

    @Autowired
    protected ExecutionContext executionContext;

    @Autowired
    protected DriverManager driverManager;

    @Autowired
    protected BaseSteps baseSteps;


    public DriverManager getDriverManager() {

        return driverManager;
    }

    @BeforeMethod(alwaysRun = true)
    public void setUpBeforeEveryMethod() {
        startUrl = getDriverManager().getExecutionContext().getHostUrl();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownAfterEveryMethod() {
        if (System.getProperty(ConfigurationProperties.DEFAULT_HUB).isEmpty()) {
            getDriverManager().handleBrowserInstance();
        } else
            getDriverManager().stop();
    }

    public String getStartUrl() {
        return executionContext.getHostUrl();
    }

}
