package com.app.test.steps;

import com.app.test.core.DriverManager;
import com.app.test.model.LoginPage;
import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.popup.dialog.PopUpDialogChanges;
import com.app.test.model.common.composite.elements.popup.notification.PopUpError;
import com.app.test.model.common.composite.elements.popup.notification.PopUpWarning;
import com.app.test.model.domain.User;
import com.app.test.model.info.InfoPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.concurrent.TimeUnit;

/**
 * Created by ALyubozhenko
 */

@Component
public class BaseSteps {

    protected final static boolean AVAILABLE = true;
    protected final static boolean UNAVAILABLE = false;

    private static final ThreadLocal<LoginPage> LOGIN_PAGE_THREAD_LOCAL = new ThreadLocal<>();

    private static final ThreadLocal<InfoPage> INFO_PAGE_THREAD_LOCAL = new ThreadLocal<>();

    private final static ThreadLocal<PopUpWarning> POPUP_WARNING_THREAD_LOCAL = new ThreadLocal<>();

    private final static ThreadLocal<PopUpError> POPUP_ERROR_THREAD_LOCAL = new ThreadLocal<>();

    private final static ThreadLocal<PopUpDialogChanges>
            POP_UP_SAVE_DIALOG_CHANGES_THREAD_LOCAL = new ThreadLocal<>();

    @Autowired
    protected DriverManager driverManager;

    public DriverManager getDriverManager() {
        return driverManager;
    }

    public PopUpWarning getPopUpWarning() {
        return POPUP_WARNING_THREAD_LOCAL.get();
    }

    public void setPopUpWarning(PopUpWarning popUpWarning) {
        POPUP_WARNING_THREAD_LOCAL.set(popUpWarning);
    }

    public PopUpError getPopUpError() {
        return POPUP_ERROR_THREAD_LOCAL.get();
    }

    public void setPopUpError(PopUpError popUpError) {
        POPUP_ERROR_THREAD_LOCAL.set(popUpError);
    }


    public LoginPage getLoginPage() {
        return LOGIN_PAGE_THREAD_LOCAL.get();
    }

    public void setLoginPage(LoginPage loginPageV) {
        LOGIN_PAGE_THREAD_LOCAL.set(loginPageV);
    }

    public InfoPage getInfoPage() {
        return INFO_PAGE_THREAD_LOCAL.get();
    }

    public void setInfoPage(InfoPage infoPageV) {
        INFO_PAGE_THREAD_LOCAL.set(infoPageV);
    }

    public void openPage(String startUrl) {
        getDriverManager().getDriverExtension().getWrappedDriver().manage().timeouts()
                .pageLoadTimeout(30, TimeUnit.SECONDS);
        getDriverManager().getDriverExtension().open(startUrl);
        setLoginPage(AbstractBase.getInitPageBean(LoginPage.class));
    }

    @Step("Authorize through the application")
    public void login(User user) {
        getLoginPage().doLogin(user.getName(), user.getPassword());
    }

    public void doLogout() {
        throw new NotImplementedException();
    }


}
