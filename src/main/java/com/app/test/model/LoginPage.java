package com.app.test.model;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */

@Component
@FindBy(css = ".login_window")
@Name("Login Window")
public class LoginPage extends AbstractBase {
    private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

    private static final String WINDOW_HEADER_CAPTION = "Some name here...";

    @FindBy(css = "")
    private WebElement captionWindow;

    @FindBy(css = ".login_normal")
    private WebElement loginField;

    @FindBy(css = ".password_normal")
    private WebElement passwordField;

    @FindBy(css = ".v-button")
    private WebElement enterButton;


    @Step
    public void doLogin(String login, String password) {
        logger.info("IN " + Thread.currentThread().getName() + " Login as user " + login);
        enterLogin(login);
        getDriverExtension().waiting(2000);
        enterPassword(password);
        getDriverExtension().waiting(1500);
        clickButton();
    }

    public WebElement getLoginField() {
        return getDriverExtension().isExistsReturnWebElement(loginField);
    }

    public WebElement getPasswordField() {
        return getDriverExtension().isExistsReturnWebElement(passwordField);
    }

    public WebElement getEnterButton() {
        return enterButton;
    }

    @Step
    private void enterLogin(String login) {

        getDriverExtension().safeEnterTextWithRepeatEnter(getLoginField(), login);
    }


    @Step
    private void enterPassword(String password) {
        getDriverExtension().safeEnterTextWithRepeatEnter(getPasswordField(), password);
    }

    @Step
    private void clickButton() {
        getDriverExtension().safeClickElement(getEnterButton());
    }

    @Override
    public Boolean isValidOpenElement() {
        return captionWindow.getText().contains(WINDOW_HEADER_CAPTION);
    }
}
