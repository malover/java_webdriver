package com.app.test.model.domain;

import com.app.test.config.ConfigurationProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by ALyubozhenko
 */
@Component
@Scope(value = "prototype")
public class User {

    private String name;
    private String password;

    private User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public static User getTestUser() {
        return new User(System.getProperty(ConfigurationProperties.TEST_USER),
                System.getProperty(ConfigurationProperties.TEST_PASSWORD));
    }

    public static User getAdminUser() {
        return new User("admin", "admin");
    }

    public static User getAltUser() {
        return new User("user-alt", "1");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
