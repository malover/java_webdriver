package com.app.test.model.domain;

/**
 * Created by ALyubozhenko
 */


public enum Branches {

    Auckland("Auckland"), Wellington("Wellington"), Christchurch("Christchurch"), Hamilton(
            "Hamilton");


    private String description;

    Branches(String description) {
        this.description = description;
    }

    public static String[] getDescriptions() {
        int size = Branches.values().length;
        String[] description = new String[size];

        for (int i = 0; i < size; i++) {
            description[i] = values()[i].getDescription();
        }

        return description;

    }

    private String getDescription() {
        return description;
    }

    public String getText() {

        return getDescription();
    }


}

