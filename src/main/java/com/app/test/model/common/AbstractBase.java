package com.app.test.model.common;

import com.app.test.config.ApplicationContextUtils;
import com.app.test.model.common.composite.elements.popup.notification.PopUpError;
import com.app.test.core.DriverExtension;
import com.app.test.core.DriverManager;
import com.app.test.model.common.composite.elements.popup.notification.PopUpWarning;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by ALyubozhenko
 */
@Component
public abstract class AbstractBase extends HtmlElement {


    protected static final String ATTRIBUTE_NAME_CLASS = "class";
    protected static final String ATTRIBUTE_NAME_VALUE = "value";
    protected static final String DISABLED_CLASSNAME = "v-disabled";

    protected static final String PROGRESS_BAR_CSS_NAME = ".v-loading-indicator";


    private static final Logger logger = LoggerFactory.getLogger(AbstractBase.class);

    /**
     * Declared as 'static' cause is called inside
     * 'public static <T extends AbstractBase> T getInitPageBean(Class<T> var1, String qualifier)'
     *
     * @return
     */
    public static DriverExtension getDriverExtension() {
        return ApplicationContextUtils.getApplicationContext().getBean(DriverManager.class)
                .getDriverExtension();
    }

    /**
     * Gets element bean and initializes this element;
     *
     * @param var1      Class of Page Object (element)
     * @param qualifier qualifier bean
     * @param <T>       page object (element) type
     * @return initialized element
     */


    public static <T extends AbstractBase> T getInitPageBean(Class<T> var1, String qualifier) {
        T result =
                HtmlElementLoader.create(var1, getDriverExtension().getWrappedDriver());
        result.afterInitPage();
        return result;
    }

    /**
     * Gets element bean and initializes this element;
     *
     * @param var1 Class of Page Object (element)
     * @param <T>  page object (element) type
     * @return initialized element
     */
    public static <T extends AbstractBase> T getInitPageBean(Class<T> var1) {
        return AbstractBase.getInitPageBean(var1, "");
    }

    /**
     * return notification popup window black color
     *
     * @return
     */


    public static PopUpWarning getWarningPopUpWindow() {
        return getInitPageBean(PopUpWarning.class);
    }

    /**
     * return notification popup window red color
     *
     * @return
     */

    public static PopUpError getErrorPopUpWindow() {
        return getInitPageBean(PopUpError.class);
    }

    protected void afterInitPage() {

    }

    public String getThread() {
        return Long.toString(Thread.currentThread().getId());
    }

    /**
     * Initializes element in context of instance OR (in other words),
     * re-initialize element according to instance type
     *
     * @param qualifier qualifier bean
     * @param instance  - Class,
     * @param <T>
     * @return
     */
    public <T extends AbstractBase> T initElementToNewClass(Class<T> instance, String qualifier) {
                logger.info(
                        "IN " + Thread.currentThread().getName() + " init element to new class: " + instance
                                .toString() + "  qualifier " + qualifier);
        T result = getInitPageBean(instance, qualifier);
        HtmlElementLoader.populatePageObject(result, new HtmlElementLocatorFactory(this));
        result.afterInitPage();
        return result;
    }

    public <T extends AbstractBase> T initElementToNewClass(Class<T> instance) {

        return initElementToNewClass(instance, "");
    }

    public abstract Boolean isValidOpenElement();


    /**
     * This method is intentionally left empty. It's declared here to hide errors in AbstractBase.class childs,
     * cause HtmlElement.class doesn't implement it in version 1.14
     *
     * It brings no harm, cause screenshoting is implemented via yandex ashot library.
     *
     *
     */
    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }

}
