package com.app.test.model.common.composite.elements.buttons;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

/**
 * Created by Alybozhenko
 */

@Component("BaseButtons")

@Name("Base class for buttons block")
@FindBy(css = ".")
public class BaseButtons
        extends AbstractBase {

    public final static boolean VISIBLE_AFTER_CLICK = true;
    public final static boolean INVISIBLE_AFTER_CLICK = false;

    @Autowired
    private List<Button> buttons;

    @Override
    public Boolean isValidOpenElement() {
        return buttons.size() > 0;
    }

    public List<Button> getButtons() {
        return buttons;
    }

    @Step("Return button from list by its class name {0}")
    public Button getButtonByClassName(String className) {
        for (Button element : getButtons()) {
            if (element.getAttribute(ATTRIBUTE_NAME_CLASS).contains(className))
                return element;
        }
        return null;
    }

    @Step("Click button {0}")
    public void clickButton(String className) {
        getButtonByClassName(className).click();
    }

}
