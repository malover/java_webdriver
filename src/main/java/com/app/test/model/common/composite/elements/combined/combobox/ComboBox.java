package com.app.test.model.common.composite.elements.combined.combobox;

import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.popup.menu.ContextWindowList;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Alybozhenko
 */
@Component
@FindBy(css = ".")
public class ComboBox extends AbstractBase {

    public static final String COMBOBOX_CLASS_NAME = "";

    @FindBy(css = "")
    private WebElement comboArrow;


    @FindBy(css = "." + (COMBOBOX_CLASS_NAME))
    private WebElement comboEdit;


    @Override
    public Boolean isValidOpenElement() {
        return null;
    }


    public WebElement getComboArrow() {
        return comboArrow;
    }

    public WebElement getComboEdit() {
        return comboEdit;
    }

    public String getText() {
        return getComboEdit().getAttribute("value");
    }


    public boolean selectValue(String textItem) {
        getDriverExtension().safeClickElement(getComboArrow());
        SuggestMenu suggestMenu = getInitPageBean(ContextWindowList.class)
                .getSuggestMenu(SuggestMenu.class,
                        ContextWindowList.FIRST_POP_UP_FILTER_WINDOW_INDEX);
        return suggestMenu != null && (!suggestMenu.isEmpty()) && suggestMenu.doSelectItem(textItem);
    }

    @Step("Set value {0}")
    public boolean setText(String text) {
        if (getText().equals(text)) {
            this.click();
            return true;
        }
        getDriverExtension().safeEnterText(getComboEdit(), text);
        getDriverExtension().waiting(2500);
        SuggestMenu suggestMenu = getInitPageBean(ContextWindowList.class)
                .getSuggestMenu(SuggestMenu.class,
                        ContextWindowList.FIRST_POP_UP_FILTER_WINDOW_INDEX);
        return suggestMenu != null && (!suggestMenu.isEmpty()) && suggestMenu.doSelectItem(text);
    }

    public String selectValue(int index) {
        getDriverExtension().safeClickElement(getComboArrow());
        String result = "";
        SuggestMenu suggestMenu = getInitPageBean(ContextWindowList.class)
                .getSuggestMenu(SuggestMenu.class,
                        ContextWindowList.FIRST_POP_UP_FILTER_WINDOW_INDEX);
        if (suggestMenu != null) {
            result = suggestMenu.doSelectItem(index);
        }
        return result;
    }

    public boolean checkValueExists(String value) {
        getDriverExtension().safeEnterText(getComboEdit(), value);
        getDriverExtension().waiting(2500);
        SuggestMenu suggestMenu = getInitPageBean(ContextWindowList.class)
                .getSuggestMenu(SuggestMenu.class,
                        ContextWindowList.FIRST_POP_UP_FILTER_WINDOW_INDEX);
        return suggestMenu != null && (!suggestMenu.isEmpty()) && suggestMenu.checkItemExists(value);

    }

    public boolean isEnabled() {
        try {
            return this.findElement(By.tagName("input")).getAttribute("disabled") == null;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
