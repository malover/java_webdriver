package com.app.test.model.common.composite.elements.table.row;

import com.app.test.model.common.composite.elements.table.row.cell.BaseCell;
import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

/**
 * Created by ALyubozhenko
 */


@Name("Base class of row in table")
public class BaseRowTable extends AbstractBase {

    private static final Logger logger = LoggerFactory.getLogger(BaseRowTable.class);

    @FindBy(css = "")
    private List<BaseCell> cells;

    public List<BaseCell> getCells() {
        return cells;
    }


    @Override
    public Boolean isValidOpenElement() {
        return getCells().size() > 0;
    }

    /**
     * return  column by index
     *
     * @param index
     * @return BaseCell
     */
    public BaseCell getCellByIndex(int index) {
        try {
            return getCells().get(index);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public BaseCell getFirstVisibleCell() {
        for (BaseCell cell : getCells()) {
            if (cell.isDisplayed())
                return cell;
        }
        return null;
    }

    @Step("Compare cell's text value {0}  with text {1}")
    public boolean isEqualsTextValue(int index, String text) {
        BaseCell cell = getCellByIndex(index);
        return cell != null && cell.getText().equalsIgnoreCase(text);

    }


}
