package com.app.test.model.common.composite.elements.popup.dialog;

import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.popup.window.BasePopUpWindow;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */

@Component
@Name("Saving changes dialog")
public class PopUpDialogChanges
        extends BasePopUpWindow {


    public static final String PROJECT_WINDOW_HEADER_CAPTION = "Save Projects changes";
    public static final String USERS_WINDOW_HEADER_CAPTION = "Save Users changes";

    @FindBy(css = ".")
    private WebElement restore;

    @FindBy(css = ".")
    private WebElement saveButton;

    public WebElement getCancel() {
        return AbstractBase.getDriverExtension().isExistsReturnWebElement(cancelButton);
    }

    public WebElement getRestore() {
        return AbstractBase.getDriverExtension().isExistsReturnWebElement(restore);
    }

    public WebElement getSave() {
        return AbstractBase.getDriverExtension().isExistsReturnWebElement(saveButton);
    }

    @Step
    public void cancelClick() {
        AbstractBase.getDriverExtension().safeClickElementAndWaitForInvisibility(getCancel());
    }

    @Step
    public void restoreClick() {
        AbstractBase.getDriverExtension().safeClickElementAndWaitForInvisibility(getRestore());
    }

    @Step
    public void saveClick() {
        AbstractBase.getDriverExtension().safeClickElementAndWaitForInvisibility(getSave());

    }


}
