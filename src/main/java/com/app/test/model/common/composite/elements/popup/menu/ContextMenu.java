package com.app.test.model.common.composite.elements.popup.menu;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

/**
 * Created by ALyubozhenko
 */
@Component
@FindBy(css = ".")
@Name("")
public class ContextMenu extends AbstractBase {

    @FindBy(css = ".")
    private List<ContextItem> items;


    @Step("Choose context menu item")
    public void clickItem(String captionItem) {
        for (ContextItem item : getItems())
            if (item.getText().equals(captionItem)) {
                getDriverExtension().safeClickElement(item);
                break;
            }
    }

    public List<ContextItem> getItems() {
        return items;
    }

    public ContextItem getLastItem() {
        return getItems().size() > 0 ? getItems().get(getItems().size() - 1) : null;
    }

    @Override
    public Boolean isValidOpenElement() {
        return getItems().size() > 0;
    }
}
