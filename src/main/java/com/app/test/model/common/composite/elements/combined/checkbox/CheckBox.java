package com.app.test.model.common.composite.elements.combined.checkbox;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

/**
 * Created by ALyubozhenko
 */
@Component
@FindBy(css = ".")
public class CheckBox extends AbstractBase {

    public static final String CHECKBOX_TYPE_NAME = "checkbox";
    public static final String CHECKED = "true";
    public static final String UNCHECKED = "false";

    public void click() {
        getDriverExtension().safeClickElement(this.findElement(By.tagName("label")));
    }

    public boolean isChecked() {

        try {
            return this.findElement(By.tagName("input")).getAttribute("checked") != null;
        } catch (NoSuchElementException e) {
            return false;
        }

    }

    public void setChecked(boolean checked) {
        if (isChecked() != checked)
            click();
    }

    @Override
    public Boolean isValidOpenElement() {
        return null;
    }


}


