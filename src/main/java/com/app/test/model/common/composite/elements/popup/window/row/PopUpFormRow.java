package com.app.test.model.common.composite.elements.popup.window.row;

/**
 * Created by ALyubozhenko
 */

import com.app.test.model.common.composite.elements.table.row.cell.BaseCell;
import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.combined.checkbox.CheckBox;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

@Component
@Name("A row in popup window")
public class PopUpFormRow extends AbstractBase {

    private static final String COMBOBOX_CLASS_NAME = "v-filterselect-input";

    @FindBy(css = ".")
    private WebElement caption;

    @FindBy(css = ".")
    private BaseCell cell;


    public String getCaption() {
        WebElement captionText = getDriverExtension().isExistsReturnWebElement(caption);
        if (captionText != null)
            return captionText.getText();
        else
            return getCell().getCheckBox().getText();
    }


    public String getText() {
        if (getCell().isCheckBox()) {
            return getCell().isChecked() ? CheckBox.CHECKED : CheckBox.UNCHECKED;
        }
        return getCell().getText();
    }

    public BaseCell getCell() {
        return cell;
    }

    public void setValue(String value) {
        getCell().setValue(value);
    }


    @Override
    public Boolean isValidOpenElement() {
        return !getCaption().isEmpty();
    }

}
