package com.app.test.model.common.composite.elements.popup.window;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by ALyubozhenko
 */

@Component

@FindBy(css = ".")
@Name("List of popups windows")
public class PopUpWindowList extends AbstractBase {
    public static final int STANDART_SLEEP_IN_MILLISECONDS = 30000;
    public static final int SMALL_SLEEP_IN_MILLISECONDS = 2000;
    private static final Logger logger = LoggerFactory.getLogger(PopUpWindowList.class);

    @FindBy(css = ".")
    private List<BasePopUpWindow> popupWindows;

    private List<BasePopUpWindow> getPopupWindows() {
        return popupWindows;
    }


    @Override
    public Boolean isValidOpenElement() {
        return getPopupWindows().size() > 0;
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class and set timeout
     *
     * @param type    inheritable class from  BasePopUpWindow
     * @param caption in header window, returns from getHeader() method
     * @param timeout time search window in browser
     * @param <T>
     * @return inherit BasePopUpWindow from all window list
     */

    public <T extends BasePopUpWindow> T getWindow(Class<T> type, String caption, int timeout) {
        return getWindow(type, caption, "", timeout);
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class and set timeout
     *
     * @param type     inheritable class from  BasePopUpWindow
     * @param captions in header window, returns from getHeader() method
     * @param timeout  time search window in browser
     * @param <T>
     * @return inherit BasePopUpWindow from all window list
     */

    public <T extends BasePopUpWindow> T getWindow(Class<T> type, int timeout, String... captions) {
        return getWindow(type, captions, "", timeout);
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class
     *
     * @param type    inheritable class from  BasePopUpWindow
     * @param caption in header window, returns from getHeader() method
     * @param <T>
     * @return inherit BasePopUpWindow from all window list
     */

    public <T extends BasePopUpWindow> T getWindow(Class<T> type, String caption) {
        return getWindow(type, caption, "");
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class
     *
     * @param type      inheritable class from  BasePopUpWindow
     * @param caption   in header window, returns from getHeader() method
     * @param qualifier name bean from Spring IoC
     * @param <T>
     * @return inherit BasePopUpWindow window from all window list
     */


    public <T extends BasePopUpWindow> T getWindow(Class<T> type, String caption,
                                                   String qualifier) {
        return getWindow(type, caption, qualifier, STANDART_SLEEP_IN_MILLISECONDS);
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class with timeout
     *
     * @param type      inheritable class from  BasePopUpWindow
     * @param caption   in header window, returns from getHeader() method
     * @param qualifier name bean from Spring IoC
     * @param timeout   time search window in browser
     * @param <T>
     * @return inherit BasePopUpWindow window from all window list
     */

    public <T extends BasePopUpWindow> T getWindow(Class<T> type, String caption, String qualifier,
                                                   int timeout) {
        return getWindow(type, new String[]{caption}, qualifier, timeout);
    }

    /**
     * return class and cast from base class BasePopUpWindow to concrete inherit from BasePopUpWindow class with timeout
     *
     * @param type      inheritable class from  BasePopUpWindow
     * @param captions  in header window, returns from getHeader() method
     * @param qualifier name bean from Spring IoC
     * @param timeout   time search window in browser
     * @param <T>
     * @return inherit BasePopUpWindow window from all window list
     */

    public <T extends BasePopUpWindow> T getWindow(Class<T> type, String[] captions,
                                                   String qualifier, int timeout) {
        final long beginTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < (beginTime + timeout)) {
            for (BasePopUpWindow popUpWindow : getPopupWindows()) {
                T window = popUpWindow.initElementToNewClass(type, qualifier);
                for (String caption : captions) {
                    WebElement windowHeader = window.getWindowHeader();
                    if (windowHeader != null && (windowHeader.getText()
                            .equals(caption))) {
                        return window;
                    }
                }
            }
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
