package com.app.test.model.common.composite.elements.table.row.cell;

import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.buttons.Button;
import com.app.test.model.common.composite.elements.combined.checkbox.CheckBox;
import com.app.test.model.common.composite.elements.combined.combobox.ComboBox;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */
@Component
@Name("Base cell class")
public class BaseCell extends AbstractBase {

    public static final String GREEN_COLOR = "v-table-cell-content-user_cmd";

    private static final String CHECKED_PICTURE = "grid-checkmark.png";
    private static final String UNCHECKED_PICTURE = "block-disabled.png";

    @FindBy(css = ".")
    private WebElement editText;

    @Autowired
    private CheckBox checkbox;

    @Autowired
    private ComboBox comboBox;

    @FindBy(tagName = "")
    private WebElement image;


    @Autowired
    private Button button;

    /**
     * @return cell in edit mode
     */

    public WebElement getEditTextElement() {
        return getDriverExtension().isExistsReturnWebElement(editText);
    }

    public ComboBox getCombobox() {
        return comboBox;
    }

    public Button getButton() {
        return button;
    }


    public WebElement getImage() {
        return getDriverExtension().isExistsReturnWebElement(image);
    }

    public String getText() {
        if (!this.getWrappedElement().getText().isEmpty())
            return this.getWrappedElement().getText();
        String result = this.getAttribute("innerText");
        if (result != null && result.isEmpty()) {
            if (getEditTextElement() != null)
                result = getEditTextElement().getAttribute(ATTRIBUTE_NAME_VALUE);
            if (isComboBox())
                result = getCombobox().getText();
        }
        return result != null ? result.trim() : "";
    }

    public void setValue(String text) {
        if (getEditTextElement() != null)
            getDriverExtension().safeEnterText(getEditTextElement(), text);
        else if (isComboBox()) {
            getCombobox().setText(text);
        } else if (isCheckBox()) {
            boolean textResult = text.equals(CheckBox.CHECKED);
            getCheckBox().setChecked(textResult);
        }

    }

    public String selectComboBoxValue(int index) {
        if (isComboBox()) {
            return getCombobox().selectValue(index);
        }
        return "";
    }

    /**
     * @return text from cell in edit mode
     */
    public String getEditText() {
        if (getEditTextElement() != null)
            return getEditTextElement().getAttribute(ATTRIBUTE_NAME_VALUE);
        else
            return "";
    }

    /**
     * @return checkbox in cell
     */

    public CheckBox getCheckBox() {
        return checkbox;
    }

    /**
     * @return checkbox status in cell
     */

    public boolean isChecked() {

        if (getImage() != null)
            return (getImage().getAttribute("src").contains(CHECKED_PICTURE) || !getImage()
                    .getAttribute("src").contains(UNCHECKED_PICTURE));
        return isCheckBox() && getCheckBox().isChecked();

    }

    public boolean isButton() {
        return getDriverExtension().isExistsReturnWebElement(getButton()) != null;

    }

    /**
     * @return result if cell include icon "not equals"
     */


    public boolean isCellDifferent() {
        return this.getAttribute(ATTRIBUTE_NAME_CLASS).contains("is-cell-different");
    }

    public boolean isCheckBox() {
        try {
            return this.findElement(By.tagName("input")).getAttribute("type")
                    .equals(CheckBox.CHECKBOX_TYPE_NAME);
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isComboBox() {
        try {
            return this.findElement(By.className(ComboBox.COMBOBOX_CLASS_NAME)) != null;
        } catch (NoSuchElementException e) {
            return false;
        }

    }

    @Step
    public void clickCell() {
        if (isButton())
            getButton().buttonClick();
        else
            this.click();
    }

    public boolean isColorGreen() {
        return this.getAttribute(ATTRIBUTE_NAME_CLASS).contains(GREEN_COLOR);
    }

    @Override
    public Boolean isValidOpenElement() {
        return true;
    }

//

}
