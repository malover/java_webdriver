package com.app.test.model.common.composite.elements.combined.switcher;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */
@Component
@Name("Radiobutton element")
@FindBy(css = ".v-radiobutton")
public class RadioButton
        extends AbstractBase {


    public boolean isChecked() {
        return this.findElement(By.tagName("input")).getAttribute("checked") != null;
    }


    @Override
    public Boolean isValidOpenElement() {
        return null;
    }
}
