package com.app.test.model.common.composite.elements.popup.notification;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */
@Component
@Name("Error popup")
@FindBy(css = ".")
public class PopUpError
        extends AbstractBase {

    @FindBy(tagName = "h1")
    private WebElement caption;

    @FindBy(tagName = "p")
    private WebElement text;

    public String getCaption() {
        return caption.getText();
    }


    public String getErrorText() {
        return text.getText();
    }

    @Override
    public Boolean isValidOpenElement() {
        return getDriverExtension().isExistsReturnWebElement(caption) != null;
    }

    public void closeError() {
        getDriverExtension().safeClickElement(this);
    }
}
