package com.app.test.model.common.composite.elements.popup.menu;


import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.combined.combobox.SuggestMenu;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created ALyubozhenko
 */
@Component
@FindBy(css = ".")
@Name("List of context windows")
public class ContextWindowList extends AbstractBase {

    public static final int FIRST_POP_UP_FILTER_WINDOW_INDEX = 0;
    public static final int SECOND_POP_UP_FILTER_WINDOW_INDEX = 1;
    private static final int SLEEP_IN_MILLISECONDS = 15000;

    @FindBy(css = ".")
    private List<ContextMenu> contextMenu;

    @FindBy(css = ".")
    private List<ContextMenu> suggestMenu;

    public List<ContextMenu> getListContextMenu() {
        return contextMenu;
    }


    /**
     * return class and cast from base class ContextMenu to concrete inherit from ContextMenu class
     *
     * @param type     inheritable class from  ContextMenu
     * @param position in list window
     * @param <T>
     * @return inherit ContextMenu window from all window list
     */

    public <T extends ContextMenu> T getContextMenu(Class<T> type, int position) {
        final long beginTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < (beginTime + SLEEP_IN_MILLISECONDS)) {
            if (position < getListContextMenu().size())
                return getListContextMenu().get(position).initElementToNewClass(type);
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * return class and cast from base class SuggestMenu to concrete inherit from SuggestMenu class
     *
     * @param type     inheritable class from  SuggestMenu
     * @param position in list window
     * @param <T>
     * @return inherit SuggestMenu from all window list
     */

    public <T extends SuggestMenu> T getSuggestMenu(Class<T> type, int position) {
        final long beginTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < (beginTime + SLEEP_IN_MILLISECONDS)) {
            if (position < getListSuggestMenu().size())
                return getListSuggestMenu().get(position).initElementToNewClass(type);
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<ContextMenu> getListSuggestMenu() {
        return suggestMenu;
    }


    @Override
    public Boolean isValidOpenElement() {
        return getListContextMenu().size() > 0;
    }


}
