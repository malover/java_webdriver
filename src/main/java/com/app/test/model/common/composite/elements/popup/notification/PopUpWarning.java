package com.app.test.model.common.composite.elements.popup.notification;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import static org.hamcrest.Matchers.not;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.exists;

/**
 * Created by ALyubozhenko
 */
@Component
@Name("Warning popup")
@FindBy(css = ".v-Notification-warning, .v-Notification")
public class PopUpWarning
        extends AbstractBase {

    @FindBy(tagName = "h1")
    private WebElement caption;

    @FindBy(tagName = "p")
    private WebElement text;


    public String getCaption() {
        return caption.getText();
    }

    public String getWarningText() {
        return text.getText();
    }

    @Override
    public Boolean isValidOpenElement() {
        return getDriverExtension().isExistsReturnWebElement(caption) != null;
    }

    public void closeWarning() {
        getDriverExtension().safeClickElement(this);
        getDriverExtension().checkUntil(this, not(exists()), 1);
    }
}
