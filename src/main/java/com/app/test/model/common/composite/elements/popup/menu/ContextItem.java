package com.app.test.model.common.composite.elements.popup.menu;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */


@Component
@Name("Context menu item")
@FindBy(css = ".")
public class ContextItem extends AbstractBase {

    @FindBy(className = "some txt")
    private WebElement caption;

    @Override
    public Boolean isValidOpenElement() {
        return caption.isDisplayed();
    }

    public String getText() {
        return caption.getText();
    }
}
