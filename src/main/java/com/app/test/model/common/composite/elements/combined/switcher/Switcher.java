package com.app.test.model.common.composite.elements.combined.switcher;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

import static ru.yandex.qatools.matchers.webdriver.AttributeMatcher.attribute;

/**
 * Created by ALyubozhenko
 */

@Component
@Name("Switcher element")
public class Switcher extends AbstractBase {

    @Autowired
    private List<RadioButton> buttons;


    public List<RadioButton> getButtons() {
        return buttons;
    }

    public void clickButton(String name) {
        for (RadioButton button : getButtons()) {
            if (button.getText().equals(name)) {
                getDriverExtension().safeClickElement(button.getWrappedElement());
                getDriverExtension().checkUntil(button.findElement(By.tagName("input")),
                        attribute("checked", "true"));
                return;
            }
        }
    }

    @Override
    public Boolean isValidOpenElement() {
        return getButtons().size() > 0;
    }
}
