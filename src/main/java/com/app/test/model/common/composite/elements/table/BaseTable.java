package com.app.test.model.common.composite.elements.table;


import com.app.test.model.common.AbstractBase;
import com.app.test.model.common.composite.elements.table.row.BaseRowTable;
import com.app.test.model.common.composite.elements.table.row.cell.BaseCell;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static ru.yandex.qatools.matchers.webdriver.AttributeMatcher.className;

/**
 * Created by ALyubozhenko
 */

@FindBy(css = ".v-table")
@Name("Base class for tables")
public class BaseTable extends AbstractBase {

    private static final String CLASSNAME_SELECTED_RECORD = "v-selected";

    private static final int ROW_HEIGHT = 24;

    @FindBy(css = ".v-table-body")
    private WebElement body;
    @FindBy(css = ".v-table-footer-wrap")
    private WebElement footer;
    @FindBy(css = "." + CLASSNAME_SELECTED_RECORD)
    private BaseRowTable activeRow;
    @FindBy(css = ".v-table-row, .v-table-row-odd")
    private List<BaseRowTable> rows;

    public int getCountRows() {
        return rows.size();
    }

    @Step("Initiate context menu call on active row")
    public void contextClickInActiveRow() {
        getDriverExtension()
                .mouseRightClickTopLeft(getActiveRow().getFirstVisibleCell().getWrappedElement());
    }

    @Step("Select records in group edit mode")
    public void multiplySelectRow(BaseRowTable row) {
        List<BaseRowTable> rows = new ArrayList<BaseRowTable>();
        rows.add(row);
        List<BaseRowTable> unmodifiableRows = Collections.unmodifiableList(rows);
        multiplySelectRows(unmodifiableRows);
    }


    @Step("Select all records in group edit mode")
    public void multiplySelectRows(List<BaseRowTable> rows) {
        for (BaseRowTable currRow : rows) {
            String rowText = currRow.getText();
            if (getActiveRow() == null || !getActiveRow().getText().equals(rowText)) {
                getDriverExtension().safeSelectElementWithCtrl(currRow.getWrappedElement());
                getDriverExtension()
                        .checkUntil(currRow, className(containsString(CLASSNAME_SELECTED_RECORD)));

            }
        }
    }

    @Step("Selecting record")
    public void selectRow(BaseRowTable row) {
        String rowText = row.getText();
        if (getActiveRow() == null || !getActiveRow().getText().equals(rowText)) {
            getDriverExtension().safeClickElementTopLeft(row.getCellByIndex(0).getWrappedElement());
            getDriverExtension()
                    .checkUntil(row, className(containsString(CLASSNAME_SELECTED_RECORD)));
        }
    }


    @Step("Selecting 1st record")
    public void selectFirstRecord() {
        selectRecords(0);
    }

    @Step("Selecting {0} record")
    public void selectRecords(int index) {
        if (!getAllRows().isEmpty())
        selectRow(getAllRows().get(index));
    }


    @Step("Scroll table down for {0} lines")
    public void scrollDown(int rows) {
        getDriverExtension().jsScrollInGrid(getBody(), Math.abs(rows * ROW_HEIGHT));

    }


    @Step("Double click by cell with index {0} in active row")
    public BaseCell doubleClickToCellByIndexForEdit(int index) {

        BaseRowTable rowTable = getActiveRow();
        int rowIndex = getRowNumber(rowTable);
        if (rowTable != null) {
            getDriverExtension()
                    .explicitDoubleClickTopLeft(rowTable.getCellByIndex(index).getWrappedElement());
            // for waiting and get Base cell object with text edit after double click;
            getDriverExtension().waiting(1500);
            rowTable = getAllRows().get(rowIndex);
            selectRow(rowTable);
            return rowTable.getCellByIndex(index);


        }
        return null;
    }

    @Step("Clear cell with index {0} in active row")
    public BaseCell clearCellByIndex(int index) {

        BaseRowTable rowTable = getActiveRow();
        if (rowTable != null) {
            getDriverExtension().clearText(rowTable.getCellByIndex(index).getWrappedElement());
            // for waiting and get Base cell object with text edit after double click;
            getDriverExtension().waiting(1000);
            return getActiveRow().getCellByIndex(index);
        }
        return null;
    }

    @Step("Ввод текста {0} в ячейку с индексом {1} активной строки ")
    public void setTextInActiveRowToCellByIndex(String text, int index) {
        getActiveRow().getCellByIndex(index).setValue(text);
    }

    @Step("Клик по чекбоксу в ячейке с индексом {0} активной строки ")
    public void clickCheckboxInActiveRowToCellByIndex(int index) {
        getActiveRow().getCellByIndex(index).getCheckBox().click();
    }

    @Step("Double click by active row")
    public void doubleClickToActiveRow() {
        getDriverExtension().explicitDoubleClick(getActiveRow().getWrappedElement());
    }



    private WebElement getBody() {
        return getDriverExtension().isExistsReturnWebElement(body);
    }

    private WebElement getFooter() {
        return getDriverExtension().isExistsReturnWebElement(footer);
    }

    public List<BaseRowTable> getAllRows() {
        return rows;
    }


    public BaseRowTable getActiveRow() {
        return getDriverExtension().isExistsReturnWebElement(activeRow.getWrappedElement())
                != null ? activeRow : null;
    }

    @Step("Return string with text {0}")
    public BaseRowTable getRowContainsText(String text) {
        int recordCountInClient = 0;
        String lastRowText = "";
        scrollDown(0);
        if (getCountRows() > 0) {
            do {
                for (BaseRowTable rowTable : getAllRows()) {
                    if (rowTable.getText().contains(text))
                        return rowTable;
                    recordCountInClient++;
                    lastRowText = rowTable.getText();
                }
                scrollDown(recordCountInClient);
            } while (!lastRowText.equals(getAllRows().get(getAllRows().size() - 1).getText()));
        }
        return null;
    }

    @Step("Comparing two rows by text contains")
    public boolean isEqualsRow(BaseRowTable one, BaseRowTable two) {
        return one.getText().equals(two.getText());
    }


    @Step("Return cell by row's index {0}  ")
    public BaseCell getCellByColumnIndex(int indexColumn, BaseRowTable rowTable) {
        return rowTable.getCellByIndex(indexColumn);
    }

    @Step("Return index of row {0}")
    public int getRowNumber(BaseRowTable row) {
        for (int result = 0; result < getAllRows().size(); result++) {
            if (isEqualsRow(getAllRows().get(result), row))
                return result;
        }
        return -1;
    }

    @Step("Setting up Checkbox for column {1} and row with text{0}")
    public BaseRowTable clickCheckboxInRow(String includeText, int checkBoxCellIndex) {
        BaseRowTable row;
        if (includeText.isEmpty()) {
            selectFirstRecord();
            row = getActiveRow();
        } else {
            row = getRowContainsText(includeText);
            selectRow(row);
        }
        if (row.getCellByIndex(checkBoxCellIndex).isCheckBox())
            row.getCellByIndex(checkBoxCellIndex).setValue("true");
        else
            getDriverExtension().safeClickElement(row.getWrappedElement());
        return row;
    }


    @Step("Setting up the checkboxes for {0} columns")
    public void clickCheckboxesInRow(int count, int checkBoxCellIndex) {
        BaseRowTable row;
        if ((!getAllRows().isEmpty()) && (getAllRows().size() >= count)) {
            for (int i = 0; i < count; i++) {
                selectRow(getAllRows().get(i));
                row = getActiveRow();
                if (row.getCellByIndex(checkBoxCellIndex).isCheckBox()) {
                    row.getCellByIndex(checkBoxCellIndex).getCheckBox().click();
                } else
                    getDriverExtension().safeClickElement(row.getWrappedElement());
            }
        }
    }

    @Override
    public Boolean isValidOpenElement() {
        return getAllRows().size() > 0;
    }


}
