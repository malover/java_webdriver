package com.app.test.model.common.composite.elements.buttons;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by Alybozhenko
 */
@Component
@FindBy(css = ".")
@Name("Button")
public class Button extends AbstractBase {

    public void buttonClick() {
        getDriverExtension().safeClickElement(this);
    }


    @Override
    public Boolean isValidOpenElement() {
        return null;
    }
}
