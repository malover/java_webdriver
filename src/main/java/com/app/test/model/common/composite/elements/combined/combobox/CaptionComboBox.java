package com.app.test.model.common.composite.elements.combined.combobox;

import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by ALyubozhenko
 */

/*
Implemented like Composition to ensure that Selenium will correctly map ComboBox and CaptionComboBox
 */

@Component("CaptionCombobox")
@FindBy(css = ".")
public class CaptionComboBox extends AbstractBase {

    @FindBy(css = ".")
    private WebElement comboBoxHeader;

    @Autowired
    private ComboBox combobox;


    @Override
    public Boolean isValidOpenElement() {
        return null;
    }

    public WebElement getComboBoxHeader() {
        return getDriverExtension().isExistsReturnWebElement(comboBoxHeader);
    }

    public ComboBox getCombobox() {
        return combobox;
    }


    public String getComboBoxText() {
        return getCombobox().getText();
    }


    public boolean selectValue(String textItem) {
        return getCombobox().selectValue(textItem);
    }


    public boolean setText(String text) {

        return getCombobox().setText(text);
    }

    public String selectValue(int index) {

        return getCombobox().selectValue(index);
    }

    public String getHeader() {
        return getComboBoxHeader() != null ? getComboBoxHeader().getText() : "";
    }
}
