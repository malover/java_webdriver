package com.app.test.model.common.composite.elements.combined.combobox;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ALyubozhenko
 */
@Component
@FindBy(css = ".")
public class SuggestMenu extends AbstractBase {

    @FindBy(css = ".")
    private WebElement previousButton;


    @FindBy(css = ".")
    private WebElement previousButtonOff;

    @FindBy(css = ".")
    private List<WebElement> items;

    @FindBy(css = ".")
    private WebElement selectedItem;


    @FindBy(css = ".")
    private WebElement nextButton;

    @FindBy(css = ".")
    private WebElement nextButtonOff;


    @FindBy(css = ".")
    private WebElement rowsStatus;


    public List<WebElement> getItems() {
        return items;
    }

    public WebElement getSelectedItem() {
        return getDriverExtension().isExistsReturnWebElement(selectedItem);
    }

    public WebElement getPreviousButton() {
        return getDriverExtension().isExistsReturnWebElement(previousButton);
    }

    public WebElement getNextButton() {
        return getDriverExtension().isExistsReturnWebElement(nextButton);
    }

    public WebElement getPreviousButtonOff() {
        return getDriverExtension().isExistsReturnWebElement(previousButtonOff);
    }

    public WebElement getNextButtonOff() {
        return getDriverExtension().isExistsReturnWebElement(nextButtonOff);
    }

    public WebElement getRowsStatus() {
        return rowsStatus;
    }

    private void clickPrevious() {
        getDriverExtension().safeClickElement(getPreviousButton());
        getDriverExtension().waiting(1000);
    }

    private void clickNext() {
        getDriverExtension().safeClickElement(getNextButton());
        getDriverExtension().waiting(1000);
    }

    /**
     * get item by text from dropdown list
     *
     * @param text      searching text
     * @return selected item in dropdown list
     */

    public WebElement getItemsByText(String text) {
        List<WebElement> items = new LinkedList<>(getItems());

        // work if list not exists prev and next arrow
        WebElement result = getItem(text, items);
        if (result != null)
            return result;

        String compareValue = getSelectedItem() != null ? getSelectedItem().getText() : text;
        // work if list begin in first position
        if (getNextButton() != null && text.compareTo(compareValue) <= 0) {
            clickNext();
            items = new LinkedList<>(getItems());
            while (getNextButton() != null) {
                result = getItem(text, items);
                if (result != null)
                    return result;
                else
                    clickNext();
            }
            return getItem(text, items);
        }

        // work if list begin in last position
        if (getPreviousButton() != null) {
            clickPrevious();
            items = new LinkedList<>(getItems());
            while (getPreviousButton() != null) {
                result = getItem(text, items);
                if (result != null)
                    return result;
                else
                    clickPrevious();
                items = new LinkedList<>(getItems());
            }
            return getItem(text, items);
        }
        return null;
    }

    private WebElement getItem(String text, List<WebElement> items) {
        for (WebElement element : items) {
            if (element.getText().equals(text))
                return element;
        }
        return null;
    }

    @Step("Choose item {0} from dropdown list")
    public boolean doSelectItem(String textItem) {
        WebElement selected = getItemsByText(textItem);
        if (selected != null) {
            getDriverExtension().safeClickElement(selected);
            return true;
        }
        else
            return doSelectItem(0).equals(textItem);
    }

    @Step("Choose dropdown menu item with index {0}")
    public String doSelectItem(int index) {
        String result = "";
        if (index < getItems().size()) {
            result = getItems().get(index).getText();
            getDriverExtension().safeClickElement(getItems().get(index));
        }
        return result;
    }

    @Step("Checking existing value with {0}")
    public boolean checkItemExists(String item) {
        return getItemsByText(item) != null;
    }

    @Step
    public boolean isEmpty() {
        return getItems().isEmpty();
    }

    @Override
    public Boolean isValidOpenElement() {
        return null;
    }
}
