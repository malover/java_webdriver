package com.app.test.model.common.composite.elements.popup.window;


import com.app.test.model.common.composite.elements.combined.combobox.CaptionComboBox;
import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

import static org.hamcrest.Matchers.not;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.exists;

/**
 * Created by ALyubozhenko
 */

@Component
@FindBy(css = ".")
@Name("Base class of popup window ")
public class BasePopUpWindow extends AbstractBase {

    protected final static String CLASSNAME_SELECTED_TAB = "v-tabsheet-tabitemcell-selected";
    @FindBy(css = ".ok_button, .save_button")
    protected WebElement addButton;
    @FindBy(css = ".cancel_button")
    protected WebElement cancelButton;
    @FindBy(css = ".v-window-outerheader")
    private WebElement windowHeader;
    @FindBy(className = "v-window-maximizebox")
    private WebElement windowMaximizeButton;
    @FindBy(className = "v-window-closebox")
    private WebElement windowCloseButton;
    @FindBy(className = "v-window-contents")
    private WebElement content;
    @FindBy(className = "v-window-footer")
    private WebElement footer;
    @Autowired
    private List<CaptionComboBox> comboBoxes;

    public WebElement getWindowHeader() {
        return getDriverExtension().isExistsReturnWebElement(windowHeader);
    }

    public WebElement getWindowMaximizeButton() {
        return getDriverExtension().isExistsReturnWebElement(windowMaximizeButton);
    }

    public WebElement getWindowCloseButton() {
        return getDriverExtension().isExistsReturnWebElement(windowCloseButton);
    }

    public WebElement getApplyButton() {
        return getDriverExtension().isExistsReturnWebElement(addButton);
    }

    public WebElement getContent() {
        return getDriverExtension().isExistsReturnWebElement(content);
    }

    public WebElement getFooter() {
        return getDriverExtension().isExistsReturnWebElement(footer);
    }


    public void closeWindow() {
        getDriverExtension().jsScrollIntoView(getWindowCloseButton());
        getDriverExtension().safeClickElementAndWait(getWindowCloseButton(), not(exists()));
    }

    public void apply() {
        getDriverExtension().jsScrollIntoView(getApplyButton());
        getDriverExtension().safeClickElementAndWait(getApplyButton(), not(exists()));
    }

    protected CaptionComboBox parseElementByHeaderName(String name) {

        for (CaptionComboBox currCombo : getComboBoxes()) {
            if (currCombo.getComboBoxHeader().getText().equalsIgnoreCase(name))
                return currCombo;
        }
        return null;
    }

    public List<CaptionComboBox> getComboBoxes() {
        return comboBoxes;
    }


    @Override
    public Boolean isValidOpenElement() {
        return getWindowHeader().getText() != null;
    }

}
