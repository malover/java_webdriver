package com.app.test.model.info;

import com.app.test.model.info.headers.BaseHeaders;
import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

/**
 * Created by ALyubozhenko
 */
@Component
@Name("Some menu of some main page")
@FindBy(className = "main_menu_area")
public class MainMenu extends AbstractBase {

    private static final int PROJECT_HEADERS_NUMBER_IN_LIST = 0;

    private static final int USER_HEADERS_NUMBER_IN_LIST = 1;

    @FindBy(css = ".")
    private List<BaseHeaders> headers;

    @Override
    public Boolean isValidOpenElement() {
        return headers.size() > 0;
    }


}
