package com.app.test.model.info.headers;

import com.app.test.model.common.AbstractBase;

/**
 * Created by ALyubozhenko
 */

public class BaseHeaders extends AbstractBase {


    @Override
    public Boolean isValidOpenElement() {
        return true;
    }
}
