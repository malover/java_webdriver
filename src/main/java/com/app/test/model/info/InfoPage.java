package com.app.test.model.info;


import com.app.test.model.common.AbstractBase;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by ALyubozhenko
 */

@Component

@Name("Application start page")
@FindBy(css = ".")
public class InfoPage
        extends AbstractBase {

    private static final Logger logger = LoggerFactory.getLogger(InfoPage.class);


    @Autowired
    private MainMenu mainMenu;

    @Override
    public Boolean isValidOpenElement() {
        return getMainMenu().isValidOpenElement();
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }

}
