package com.app.test.config;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;


/**
 * Created by ALyubozhenko
 */
public final class TestConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(TestConfiguration.class);
    private static final String CONFIG_FILE_NAME = "global.properties";

    private static PropertiesConfiguration read() {

        PropertiesConfiguration configuration;
        try {
            configuration = new PropertiesConfiguration();
            configuration.setEncoding("UTF-8");
            configuration.load(CONFIG_FILE_NAME);
        } catch (ConfigurationException e) {
            throw new RuntimeException("Cannot obtain config", e);
        }

        return configuration;
    }

    public static void setSystemProperties() {
        final PropertiesConfiguration configuration = read();

        if (System.getProperty(ConfigurationProperties.MAIN_URL) == null) {
            System.setProperty(ConfigurationProperties.MAIN_URL,
                    configuration.getString("main_url"));
        }
        System.setProperty(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT,
                configuration.getString("wait_timeout"));
        System.setProperty("webdriver.chrome.driver",
                buildPathToProject(configuration.getString("chrome_driver_path")));
        System.setProperty("webdriver.ie.driver", configuration.getString("ie_driver_path"));
        System.setProperty(ConfigurationProperties.BROWSER_TYPE,
                configuration.getString("browser_type"));
        System.setProperty(ConfigurationProperties.TEST_USER, configuration.getString("test_user"));
        System.setProperty(ConfigurationProperties.TEST_PASSWORD,
                configuration.getString("test_password"));
        System.setProperty(ConfigurationProperties.BROWSER_WINDOW_SIZE,
                configuration.getString("window_size"));
        if (System.getProperty(ConfigurationProperties.DEFAULT_HUB) == null) {
            System.setProperty(ConfigurationProperties.DEFAULT_HUB,
                    configuration.getString("default_hub"));
        }
    }


    private static String buildPathToProject(String localDriverPath) {
        StringBuffer driverPath = new StringBuffer();
        String targetPath =
                TestConfiguration.class.getProtectionDomain().getCodeSource().getLocation()
                        .getPath();
        int targetIndex = targetPath.indexOf("target");
        driverPath.append(targetPath.substring(0, targetIndex));
        driverPath.append(localDriverPath);
        try {
            return URLDecoder.decode(driverPath.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            logger.info("was unable to decode driver path safely");
        }
        return driverPath.toString();
    }

}
