package com.app.test.config;

/**
 * Created by ALyubozhenko
 */
public class ConfigurationProperties {
    public static final String MAIN_URL = "main_url";
    public static final String WAIT_FOR_ELEMENT_TIMEOUT = "wait_timeout";
    public static final String BROWSER_TYPE = "browser_type";
    public static final String TEST_USER = "test_user";
    public static final String TEST_PASSWORD = "test_password";
    public static final String BROWSER_WINDOW_SIZE = "window_size";
    public static final String DEFAULT_HUB = "default_hub";

}
