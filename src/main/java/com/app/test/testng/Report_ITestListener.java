package com.app.test.testng;

import com.app.test.config.ApplicationContextUtils;
import com.app.test.core.DriverManager;
import com.app.test.utils.ScreenshotUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * Created by ALyubozhenko
 */
public class Report_ITestListener implements ITestListener {
    private static final Logger logger = LoggerFactory.getLogger(Report_ITestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        logger.info("IN " + Thread.currentThread().getName() + " launching " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("IN " + Thread.currentThread().getName() + " passed " + result.getName());

    }

    @Override
    public void onTestFailure(ITestResult result) {
        if (!result.isSuccess()) {
            DriverManager driverManager =
                    ApplicationContextUtils.getApplicationContext().getBean(DriverManager.class);
            ScreenshotUtils.makeScreenShotWithAllure(
                    driverManager.getDriverExtension().getWrappedDriver());
        }
        logger.info("IN " + Thread.currentThread().getName() + " failed " + result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("IN " + Thread.currentThread().getName() + " skipped " + result.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {


    }

    @Override
    public void onFinish(ITestContext context) {

    }


}
