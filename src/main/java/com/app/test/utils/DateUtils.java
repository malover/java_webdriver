package com.app.test.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ALyubozhenko
 */
public final class DateUtils {

    public static SimpleDateFormat getENDateFormatter() {
        return new SimpleDateFormat("MM/dd/yy");
    }

    public static SimpleDateFormat getRUDateFormatter() {
        return new SimpleDateFormat("dd.MM.yyyy");
    }

    public static String setDate(DateTime date) {
        return getENDateFormatter().format(new Date(date.getMillis()));

    }

    public static DateTime getDateTime(String pattern, String date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.parseDateTime(date);
    }

    public static DateTime addDays(String pattern, String date, int numDays) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        return formatter.parseDateTime(date).plusDays(numDays);
    }
}
