package com.app.test.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ALyubozhenko
 */
public final class StringUtils {

    private StringUtils() {

    }

    public static String getEncodedString(String text) {
        String utfString = null;
        try {
            utfString = new String(text.getBytes(Charset.forName("cp1251")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return utfString;
    }

    public static boolean hasSpaces(String s) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(s);
        return matcher.find();
    }

}
