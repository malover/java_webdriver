package com.app.test.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by ALyubozhenko
 */
public final class ScreenshotUtils {

    private static final Logger logger = LoggerFactory.getLogger(ScreenshotUtils.class);

    private static final int TIMEOUT_TO_SCROLL = 2;
    private static final String DEFAULT_FORMAT = "png";

    @Attachment(value = " Test screenshot {0}",
            type = "image/png")
    private static byte[] saveScreenshot(byte[] screenShot) {
        return screenShot;
    }


    private static Screenshot makeScreenShotOfCurrentPage(WebDriver webDriver) {
        return new AShot().shootingStrategy(new ViewportPastingStrategy(TIMEOUT_TO_SCROLL))
                .takeScreenshot(webDriver);
    }

    private static Screenshot makeScreenShotOfFailedPageArea(WebDriver webDriver,
                                                             WebElement faultElement) {
        return new AShot().shootingStrategy(new ViewportPastingStrategy(TIMEOUT_TO_SCROLL))
                .takeScreenshot(webDriver, faultElement);
    }

    public static void makeScreenShotWithAllure(WebDriver webDriver) {
        Screenshot currentShot = makeScreenShotOfCurrentPage(webDriver);

        saveScreenshot(convertImageToByteArray(currentShot.getImage()));
    }

    public static void makeElementScreenShotWithAllure(WebDriver webDriver,
                                                       WebElement faultElement) {
        Screenshot currentShot = makeScreenShotOfFailedPageArea(webDriver, faultElement);
        saveScreenshot(convertImageToByteArray(currentShot.getImage()));
    }


    private static byte[] convertImageToByteArray(BufferedImage image) {
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, DEFAULT_FORMAT, byteArrayStream);
            logger.info("Successfully wrote screenshot to byte array output stream");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("ERROR: Cannot write screenshot to ByteArrayOutputStream");
        } finally {
            try {
                byteArrayStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                byteArrayStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        byte[] imageBytes = byteArrayStream.toByteArray();

        if (imageBytes.length == 0) {
            String errorMessage = "ERROR: Converted byte array for screenshot is empty.";
            logger.error(errorMessage);
            throw new RuntimeException(errorMessage);
        }
        logger.info("Converted image screenshot to byte array. Byte array size is: "
                + imageBytes.length);

        return imageBytes;
    }

}
