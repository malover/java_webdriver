package com.app.test.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by ALyubozhenko
 */
public final class CommonDriverUtils {


    public static void scrollElementByCSS(WebDriver driver, String cssSelectorForScroll) {
        scrollElementByCSS(driver, cssSelectorForScroll, 0);
    }

    public static void scrollElementByCSS(WebDriver driver, String cssSelectorForScroll,
                                          int nextStep) {
        ((JavascriptExecutor) driver).executeScript(
                "var el = document.querySelector(\"" + cssSelectorForScroll + "\");el.scrollTop = "
                        + String.valueOf(nextStep) + ";");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean waitForJQueryProcessing(WebDriver driver, int timeOutInSeconds) {
        boolean jQcondition = false;
        try {
            new WebDriverWait(driver, timeOutInSeconds) {
            }.until(new ExpectedCondition<Boolean>() {

                @Override
                public Boolean apply(WebDriver driverObject) {
                    return (Boolean) ((JavascriptExecutor) driverObject)
                            .executeScript("return jQuery.active == 0");
                }
            });
            jQcondition = (Boolean) ((JavascriptExecutor) driver)
                    .executeScript("return window.jQuery != undefined && jQuery.active === 0");
            return jQcondition;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jQcondition;
    }


}
