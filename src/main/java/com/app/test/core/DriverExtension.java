package com.app.test.core;

import com.app.test.config.ConfigurationProperties;
import com.app.test.utils.CommonDriverUtils;
import com.app.test.utils.ScreenshotUtils;
import com.app.test.config.TestConfiguration;
import org.hamcrest.Matcher;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.stqa.selenium.factory.WebDriverFactory;
import ru.stqa.selenium.factory.WebDriverFactoryMode;
import ru.yandex.qatools.allure.annotations.Step;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNot.not;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.should;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.timeoutHasExpired;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.*;
import static ru.yandex.qatools.matchers.webdriver.DisplayedMatcher.displayed;
import static ru.yandex.qatools.matchers.webdriver.EnabledMatcher.enabled;

/**
 * Created by ALyubozhenko
 * <p/>
 * DriverExtension.class contains methods, that wrap corresponding driver methods
 */
@Component
public class DriverExtension implements WrapsDriver {
    public static final int WEB_DRIVER_WAIT = 30;
    private static final Logger logger = LoggerFactory.getLogger(DriverExtension.class);
    private static final long POOLING_INTERVAL_MILLISECONDS = 500;
    private String browserType;

    @PostConstruct
    protected void init() {
        TestConfiguration.setSystemProperties();
        browserType = System.getProperty(ConfigurationProperties.BROWSER_TYPE);
        if ((System.getProperty(ConfigurationProperties.DEFAULT_HUB) != null) && (!System
                .getProperty(ConfigurationProperties.DEFAULT_HUB).isEmpty())) {
            WebDriverFactory.setMode(WebDriverFactoryMode.THREADLOCAL_SINGLETON);
            WebDriverFactory.setDefaultHub(System.getProperty(ConfigurationProperties.DEFAULT_HUB));
            logger.info("#############    THREADLOCAL_SINGLTONE MODE  is ON!#################### ");
            logger.info("#############    GRID HUB IS ON    !#################### ");
            //            WebDriverFactory.setDefaultHub("http://172.30.44.111:4444/wd/hub");
            //            System.setProperty("webdriver.remote.server", "http://172.30.44.111:4444/wd/hub");
        }

        else{
            WebDriverFactory.setMode(WebDriverFactoryMode.SINGLETON);
        }

            Class<WebDriverFactory> clazz = WebDriverFactory.class;
            Field currField = null;
            try {
                currField = clazz.getDeclaredField("factoryInternal");
                currField.setAccessible(true);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            try {
                Field defaultHubVar = currField.getType().getDeclaredField("defaultHub");
                defaultHubVar.setAccessible(true);
                String defaultHubStr = (String) defaultHubVar.get(currField.get(null));
                logger.info("hub is set to: " + defaultHubStr);

                logger.info(currField.get(null).getClass().getName() + " driver storage is used...");

            } catch (IllegalAccessException e) {
                e.printStackTrace();

            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
    }

    public String getBrowserType() {
        return browserType;
    }

    @Override
    public WebDriver getWrappedDriver() {
        return BrowserTypeFactory.createBrowser(getBrowserType());
    }

    protected void stop() {
        BrowserTypeFactory.closeBrowser(getWrappedDriver());
    }

    public void clearAllCookies() {
        getWrappedDriver().manage().deleteAllCookies();
    }

    void click(WebElement webElement) {
        webElement.click();
    }

    @Step
    public void open(String url) {
        getWrappedDriver().get(url);
    }


    /*method will be removed soon! Try your best not to use is.
    Use WebDriverWait instead, waiting for the element of page to load*/
    @Deprecated
    public void waitForPageToLoad(long waitPeriod) {
        try {
            TimeUnit.SECONDS.sleep(waitPeriod);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * use for delay if needed
     *
     * @param waitPeriod in ms
     */
    public void waiting(long waitPeriod) {
        try {
            TimeUnit.MILLISECONDS.sleep(waitPeriod);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    /**
     * Do your best not to use this method. Will be removed soon
     * Dont change body method!!!!!!!! Because broken logic work. Or delete method and refactor
     */ public WebElement findElement(By by) {
        return getWrappedDriver().findElement(by);
    }

    public List<WebElement> findElements(By by) {
        return getWrappedDriver().findElements(by);
    }


    public void safeClick(By by) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " safe click  " + by.toString() + "  /  "
                        + this.toString());
        WebElement webElement = findElement(by);
        if (webElement != null) {
            this.click(webElement);
        } else {
            logger.info("Element: " + by + ", is not available on page - " + getWrappedDriver()
                    .getCurrentUrl());
        }
    }


    public void safeClickElement(WebElement element) {
        try {
            if (element != null) {
                logger.info(
                        "IN " + Thread.currentThread().getName() + " safe click element " + element
                                .toString() + " / " + this.toString() + " / " + getWrappedDriver()
                                .toString());
                checkUntil(element, enabled());
                element.click();
            }
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't click  at the page ");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * Use if element contains inside other element in center
     * for click to parent element because safeClickElement() clickable in center
     *
     * @param element
     */
    public void safeClickElementTopLeft(WebElement element) {
        logger.info("IN " + Thread.currentThread().getName() + " safeClickElementTopLeft " + element
                .toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).moveToElement(element, 1, 1).click().build().perform();

        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Use select row in group mode
     *
     * @param element
     */
    public void safeSelectElementWithCtrl(WebElement element) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " safeClickElementWithCtrl " + element
                .toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).keyDown(Keys.LEFT_CONTROL).click(element)
                    .keyDown(Keys.LEFT_CONTROL).build().perform();

        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void safeClickElementTopRight(WebElement element) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " safeClickElementTopRight " + element
                        .toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver())
                    .moveToElement(element, element.getSize().getWidth() - 1, 1).click().build()
                    .perform();

        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Safe click on WebElement and wait for element to become invisible
     *
     * @param webElement
     */

    public void safeClickElementAndWaitForInvisibility(WebElement webElement) {
        safeClickElementAndWait(webElement, not(displayed()));
    }

    /**
     * Safe click on WebElement and wait with matchers for WebElement
     *
     * @param webElement
     * @param matcher
     */

    public void safeClickElementAndWait(WebElement webElement, final Matcher matcher) {
        try {
            if (webElement != null) {
                logger.info("IN " + Thread.currentThread().getName() + " safeClickElementAndWait "
                        + webElement.toString() + "  /  " + this.toString());
                safeClickElement(webElement);
                long timeout = Long.valueOf(System.getProperty(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT));
                getWrappedDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
                if (matcher != null) {
                    checkUntil(webElement, matcher);
                }
                getWrappedDriver().manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
            }
        } catch (AssertionError e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), webElement);
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Deprecated //use safeClickElementAndWait
    public void safeClickElementDelay(WebElement element) {
        safeClickElement(element);
        waitForPageToLoad(2);
    }

    public void safeClickElement(By by) {
        logger.info("IN " + Thread.currentThread().getName() + " safeClickElement " + by.toString()
                + "  /  " + this.toString());
        try {
            WebElement element = getWrappedDriver().findElement(by);
            checkUntil(element, exists());
            element.click();
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void explicitClick(WebElement element) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " explicitClick " + element.toString()
                        + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).moveToElement(element).click().build().perform();
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void explicitClick(By by) {
        logger.info("IN " + Thread.currentThread().getName() + " explicitClick " + by.toString()
                + "  /  " + this.toString());
        try {
            WebElement element = getWrappedDriver().findElement(by);
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).moveToElement(element).click().build().perform();
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void explicitDoubleClick(WebElement element) {
        logger.info("IN " + Thread.currentThread().getName() + " explicitDoubleClick " + element
                .toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).clickAndHold(element).doubleClick().build().perform();
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void explicitDoubleClickTopLeft(WebElement element) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " explicitDoubleClickTopLeft " + element
                        .toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            new Actions(getWrappedDriver()).moveToElement(element, 1, 1).doubleClick().build()
                    .perform();

        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }


    public void jsClick(WebElement element) {
        logger.info("IN " + Thread.currentThread().getName() + " jsClick " + element.toString()
                + "  /  " + this.toString());
        try {
            JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
            CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
            executor.executeScript("arguments[0].click();", element);
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public boolean jsIsDisplayed(String by) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " jsIsDisplayedCheck " + by.toString()
                        + "  /  " + this.toString());
        try {
            JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
            CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
            Boolean isExists = (Boolean) executor.executeScript("isDisplayed('" + by + "');");
            return isExists.booleanValue();
        } catch (NoSuchElementException e) {
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public void jsClick(By by) {
        logger.info("IN " + Thread.currentThread().getName() + " jsClick " + by.toString() + "  /  "
                + this.toString());
        try {
            WebElement element = getWrappedDriver().findElement(by);
            CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
            JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
            executor.executeScript("arguments[0].click();", element);
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Scrolled element if it invisible to visible zone
     *
     * @param element
     */
    public void jsScrollIntoView(WebElement element) {
        try {
            CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
            JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
            executor.executeScript("arguments[0].scrollIntoView(true);", element);
            waitForPageToLoad(1);
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void jsScrollInGrid(WebElement element, int step) {
        try {
            CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
            JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
            if (step >= 0)
                executor.executeScript("arguments[0].scrollTop=" + String.valueOf(step) + ";",
                        element);
            else
                executor.executeScript("arguments[0].scrollBottom=" + String.valueOf(step) + ";",
                        element);
            waiting(2500);
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * repeat enter text if text in element  not equals keys
     *
     * @param element
     * @param keys
     */
    public void safeEnterTextWithRepeatEnter(WebElement element, String keys) {
        logger.info("IN " + Thread.currentThread().getName() + " safeEnterTextWithRepeatEnter "
                + element.toString() + "  /  " + this.toString());
        try {
            checkUntil(element, exists());
            element.click();
            waitForPageToLoad(1);
            element.clear();
            element.sendKeys(keys);
            int attempts = 5;
            // Check whether input field is blank
            if (!element.getAttribute("value").equalsIgnoreCase(keys)) {
                Actions act = new Actions(getWrappedDriver());
                for (int i = 0; i < attempts; i++) {
                    element.click();
                    waitForPageToLoad(1);
                    element.clear();
                    act.moveToElement(element).click().sendKeys(keys).build().perform();
                    if (element.getAttribute("value").equalsIgnoreCase(keys)) {
                        break;
                    }
                    if (i == attempts - 1) {
                        logger.error("Failed to enter " + keys + "for " + element.getText());
                    }
                }
            }
            checkUntil(element, anyOf(hasText(keys), hasValue(keys)));
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (AssertionError error) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" text not verify ");
            logger.error(error.getMessage());
            error.printStackTrace();
        }
    }

    /**
     * Enter text in to editable area with check wait for result. Use explicite wait
     *
     * @param element
     * @param keys
     */
    public void safeEnterText(WebElement element, String keys) {
        safeEnterText(element, keys, false);
    }

    /**
     * Enter text in to editable area with check wait for result and send return key. Use explicite wait
     *
     * @param element
     * @param keys
     * @param withReturnKey
     */

    public void safeEnterText(WebElement element, String keys, boolean withReturnKey) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " safeEnterText " + element.toString()
                        + " in element " + keys + "  /  " + this.toString());
        try {
            checkUntil(element, isEnabled());
            int tryInput = 5; //count repeat input text
            while (!element.getAttribute("value").equalsIgnoreCase(keys) && tryInput > 0) {
                ///  element.click();
                safeClickElementTopLeft(element);
                element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
                /// magic waiting don't delete
                waiting(220);
                element.sendKeys(keys);
                tryInput--;
            }
            checkUntil(element, anyOf(hasText(keys), hasValue(keys)));
            if (withReturnKey)
                element.sendKeys(Keys.chord(Keys.RETURN));
        } catch (AssertionError error) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" text not verify ");
            logger.error(error.getMessage());
            error.printStackTrace();
        }
    }

    /**
     * clear text in to editable area with check wait for result. Use explicite wait
     *
     * @param element
     */
    public void clearText(WebElement element) {
        logger.info("IN " + Thread.currentThread().getName() + " clear text " + element.toString()
                + " in element " + this.toString());
        try {
            checkUntil(element, isEnabled());
            element.click();
            element.clear();
            /// magic waiting don't delete
            waiting(220);
            checkUntil(element, anyOf(hasText(""), hasValue("")));
        } catch (AssertionError error) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" text not verify ");
            logger.error(error.getMessage());
            error.printStackTrace();
        }
    }

    public boolean isExists(By by) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " isExists " + by.toString() + "  /  "
                        + this.toString());
        boolean exists = false;
        try {
            findElement(by);
            exists = true;
        } catch (NoSuchElementException e) {
            logger.info("Element does not exist");
            logger.info(e.getMessage());
        }

        return exists;
    }

    @Deprecated
    public boolean isExistsSafe(By locator) {
        logger.info("IN " + Thread.currentThread().getName() + " isExistsSafe " + locator.toString()
                + "  /  " + this.toString());
        boolean exists = false;
        try {
            WebElement element = getWrappedDriver().findElement(locator);
            checkUntil(element, exists());
            exists = true;
        } catch (org.openqa.selenium.TimeoutException e) {
            logger.info(" element isn't present at the page");
        }
        return exists;
    }

    public boolean isExistTextInElementSafe(WebElement element, String text) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " isExistTextInElementSafe " + element
                        .toString() + "  /  " + this.toString());
        boolean exists = false;
        try {
            checkUntil(element,
                    anyOf(hasText(containsString(text)), hasValue(containsString(text))));
            exists = true;
        } catch (AssertionError e) {
            logger.info(" element isn't present at the page");
        }
        return exists;
    }



    public boolean getElementState(WebElement element) {
        return element.isEnabled();
    }

    public boolean changeElementHiddenProperty(WebElement element, boolean isInvisible) {
        JavascriptExecutor js;
        js = (JavascriptExecutor) getWrappedDriver();
        CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
        js.executeScript("arguments[0].hidden = \"" + isInvisible + "\";", element);
        return isInvisible;
    }

    public void hideElement(WebElement element) {
        JavascriptExecutor js;
        js = (JavascriptExecutor) getWrappedDriver();
        checkUntil(element, exists());

        CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
        js.executeScript("$(arguments[0]).hide();", element);
    }

    public void showElement(WebElement element) {
        JavascriptExecutor js;
        js = (JavascriptExecutor) getWrappedDriver();
        CommonDriverUtils.waitForJQueryProcessing(getWrappedDriver(), WEB_DRIVER_WAIT);
        js.executeScript("$(arguments[0]).show();", element);
    }

    public int getColumnNumber(String columnName, String locator) {
        logger.info("IN " + Thread.currentThread().getName() + " element exists " + "  /  " + this
                .toString());
        try {
            int num = 0;
            List<WebElement> listOfColumns = getWrappedDriver().findElements(By.xpath(locator));
            for (WebElement el : listOfColumns) {
                num += 1;
                if (columnName.equals(el.getText()))
                    return num;
            }
            return 0;
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new AssertionError("Element does not exist");
        }
    }

    @Deprecated
    /* will be deleted soon
    */
    public boolean xpathShouldMatchXTimes(int number, String locator) {
        List<WebElement> listOfRows = getWrappedDriver().findElements(By.xpath(locator));
        int n = listOfRows.size();
        if (n == number)
            return true;
        else
            return false;
    }

    @Deprecated
    /* will be deleted soon
    */
    public int getMatchingXpathCount(String locator) {
        List<WebElement> listOfRows = getWrappedDriver().findElements(By.xpath(locator));
        int n = listOfRows.size();
        return n;
    }

    public void mouseRightClick(WebElement element) {
        logger.info(
                "IN " + Thread.currentThread().getName() + " element exists " + element.toString()
                        + "  /  " + this.toString());
        try {
            Actions event = new Actions(getWrappedDriver());
            event.contextClick(element).build().perform();
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            throw new AssertionError("Element does not exist");
        }
    }

    public void mouseRightClickTopLeft(WebElement element) {
        logger.info("IN " + Thread.currentThread().getName() + " mouseRightClickTopLeft " + element
                .toString() + "  /  " + this.toString());
        try {
            Actions event = new Actions(getWrappedDriver());
            event.moveToElement(element, 1, 1).contextClick().build().perform();
        } catch (NoSuchElementException e) {
            ScreenshotUtils.makeElementScreenShotWithAllure(getWrappedDriver(), element);
            logger.info(" element isn't present at the page");
            logger.error(e.getMessage());
            throw new AssertionError("Element does not exist");
        }
    }

    public void mouseRightClickDelay(WebElement element) {
        mouseRightClick(element);
        waitForPageToLoad(3);
    }

    public WebElement isExistsReturnWebElement(WebElement element) {
        WebElement result;
        long timeout = Long.valueOf(System.getProperty(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT));
        try {
            getWrappedDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
            WebDriverWait wait =
                    new WebDriverWait(getWrappedDriver(), 5); // don't change this magic number!!!!
            wait.until(ExpectedConditions.visibilityOf(element));
            result = element;
        } catch (TimeoutException e) {
            logger.info(" element doesn't exist");
            result = null;
        }
        getWrappedDriver().manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        return result;
    }

    /**
     * wait until actual equals to matcher
     *
     * @param actual
     * @param matcher
     * @param timeMilliseconds max interval wait
     * @param <T>
     */

    public <T> void checkUntil(T actual, final Matcher<? super T> matcher, long timeMilliseconds) {

        assertThat(actual, should(matcher).whileWaitingUntil(timeoutHasExpired(timeMilliseconds)
                .withPollingInterval(POOLING_INTERVAL_MILLISECONDS)));
    }

    public <T> void checkUntil(T actual, final Matcher<? super T> matcher) {
        checkUntil(actual, matcher, TimeUnit.SECONDS.toMillis(WEB_DRIVER_WAIT));
    }



}
