package com.app.test.core.selenium;

import com.google.common.base.Predicate;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Clock;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;


/**
 * Created by ALyubozhenko
 */
public class WebElementWait extends FluentWait<WebElement> {
    public static Predicate<WebElement> isDisplayed = new Predicate<WebElement>() {
        @Override
        public boolean apply(WebElement element) {
            return element.isDisplayed();
        }


    };

    public WebElementWait(WebElement input) {
        super(input);
    }

    public WebElementWait(WebElement input, Clock clock, Sleeper sleeper) {
        super(input, clock, sleeper);
    }
}
