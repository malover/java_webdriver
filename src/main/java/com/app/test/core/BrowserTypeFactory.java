package com.app.test.core;

import com.app.test.config.ConfigurationProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.stqa.selenium.factory.WebDriverFactory;

/**
 * Created by ALyubozhenko
 */

class BrowserTypeFactory {
    private static final Logger logger = LoggerFactory.getLogger(BrowserTypeFactory.class);

    /**
     * Creates local web driver
     *
     * @param browserString browser name (chrome, firefox, ie, opera)
     * @return WebDriver instance
     */
    static WebDriver createBrowser(String browserString) {
        DesiredCapabilities driverType = null;
        BrowserType browserType = getBrowserType(browserString);
        WebDriver driver = null;
        switch (browserType) {
            case CHROME:
                driverType = DesiredCapabilities.chrome();
                break;
            case FIREFOX3:
                driverType = DesiredCapabilities.firefox();
                break;
            case IE:
                driverType = DesiredCapabilities.internetExplorer();
                break;
            default:
                logger.info("Unsupported browser type " + browserType);
        }
        if (driverType != null) {
            driver = WebDriverFactory.getDriver(driverType);
            if (System.getProperty(ConfigurationProperties.BROWSER_WINDOW_SIZE).equals("max"))
                driver.manage().window().maximize();
        }
        return driver;
    }

    static void closeBrowser(WebDriver webDriver) {
        WebDriverFactory.dismissDriver(webDriver);
    }


    private static BrowserType getBrowserType(String browserString) {
        return BrowserType.fromString(browserString);
    }

}

