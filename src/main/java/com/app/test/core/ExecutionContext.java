package com.app.test.core;


import org.springframework.stereotype.Component;

/**
 * Created by ALyubozhenko
 */
@Component
public class ExecutionContext {
    private String hostUrl;
    private String type;

    public String getType() {
        return type;
    }

    protected ExecutionContext setType(String type) {
        this.type = type;
        return this;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    protected ExecutionContext setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
        return this;
    }

}
