package com.app.test.core;

import com.app.test.config.ApplicationContextUtils;
import com.app.test.config.ConfigurationProperties;
import com.app.test.config.TestConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by ALyubozhenko
 * <p/>
 * Class contains only driver initialization and stop methods.
 * <p/>
 * Driver methods, working with controls is in @DriverExtension.class
 */
@Component
public class DriverManager {
    protected static final String DEFAULT_TIMEOUT = "2";
    private static final Logger logger = LoggerFactory.getLogger(DriverManager.class);
    private String waitForElementTimeout = DEFAULT_TIMEOUT;
    private long waitElementTimeout;
    private ThreadLocal<DriverExtension> driverExtension = new ThreadLocal<>();

    @Autowired
    private ExecutionContext context;

    /**
     * init browser with all params, according to specified type
     */

    @PostConstruct
    private void init() {
        TestConfiguration.setSystemProperties();
        context.setType(System.getProperty(ConfigurationProperties.BROWSER_TYPE)).
                setHostUrl(System.getProperty(ConfigurationProperties.MAIN_URL));
        waitForElementTimeout =
                System.getProperty(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT);
        waitElementTimeout = Long.valueOf(waitForElementTimeout);
    }

    /**
     * Get Selenium elements
     *
     * @return Selenium elements
     */
    public DriverExtension getDriverExtension() {
        if (driverExtension.get() == null)
            driverExtension.set(ApplicationContextUtils.getBean(DriverExtension.class));
        return driverExtension.get();
    }

    /**
     * Stop Selenium if started
     */
    public void stop() {
        logger.info("IN " + Thread.currentThread().getName() + " Selenium stop");
        getDriverExtension().stop();
        driverExtension.remove();

    }

    public void handleBrowserInstance() {
        logger.info("IN " + Thread.currentThread().getName()
                + " CLEARCOOKIE!!!");
        getDriverExtension().clearAllCookies();
    }

    public ExecutionContext getExecutionContext() {
        return context;
    }

}
