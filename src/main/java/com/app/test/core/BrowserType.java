package com.app.test.core;

/**
 * Created by ALyubozhenko
 */

public enum BrowserType {
    CHROME,
    FIREFOX3,
    IE,
    HTMLUNIT;

    /**
     * @param value string value of browser name
     * @return corresponding enum elements
     */
    public static BrowserType fromString(String value) {
        return BrowserType.valueOf(value.trim().toUpperCase());
    }
}