# README #
### Quick summary ###

Test automation project, that shows the ability to automate UI application.

Approach: Java 1.7 + maven + testng + allure(to build report + bdd) + htmlelements (to build page object) + Spring (as DI framework) + ready for selenium grid.